import firebase from './firebase';

const groupRef = firebase
  .database()
  .ref()
  .child('group');

export default {
  group: {
    fetchAll() {
      groupRef.on('value', snap => snap.val());
    }
  }
};
