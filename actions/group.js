import { getChats, setReadBy } from './chat';
import { getGroupIds } from './user';
import { closeMemberToggle } from './meta';
import { GROUP_LOADED, GROUP_SELECTED, MEMBER_LIST_ID_LOADED } from '../types';
import firebase from '../firebase';

export const groupSelected = groupId => ({
  type: GROUP_SELECTED,
  groupId
});

export const groupLoaded = groups => ({
  type: GROUP_LOADED,
  groups
});

export const memberListIdLoaded = memberList => ({
  type: MEMBER_LIST_ID_LOADED,
  memberList
});

export const getGroupByIds = groupIds => (dispatch) => {
  const groups = {};
  const groupRef = firebase
    .database()
    .ref()
    .child('group');

  groupIds.forEach((groupId) => {
    groupRef.child(groupId).on('value', (snap) => {
      groups[groupId] = snap.val();
      if (Object.keys(groups).length === groupIds.length) {
        dispatch(groupLoaded(groups));
      }
    });
  });
};

// TODO: Use groupId as parameter
export const getMemberList = groupId => () =>
  new Promise((resolve) => {
    const groupIdRef = firebase
      .database()
      .ref()
      .child(`group/${groupId}/members`);

    groupIdRef.on('value', (snap) => {
      resolve(snap.val());
    });
  });

export const getGroups = userId => (dispatch) => {
  const groupRef = firebase
    .database()
    .ref()
    .child('group');

  groupRef.on('value', () => {
    dispatch(getGroupIds(userId));
  });
};

export const selectGroup = (userId, groupId) => (dispatch) => {
  dispatch(closeMemberToggle());
  dispatch(groupSelected(groupId));
  dispatch(setReadBy(userId, groupId));
  dispatch(getChats(userId, groupId));
};
