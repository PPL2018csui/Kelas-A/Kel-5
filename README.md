[![build status](https://gitlab.com/PPL2018csui/Kelas-A/Kel-5/badges/sit_uat_react/build.svg)](https://gitlab.com/PPL2018csui/Kelas-A/Kel-5/tree/sit_uat_react)
[![codecov](https://codecov.io/gl/Kelas-A/Kel-5/branch/sit_uat_react/graph/badge.svg)](https://codecov.io/gl/Kelas-A/Kel-5/branch/sit_uat_react)

# Welcome Aboard

> "Expect the best, plan for the worst, and prepare to be surprised." ~ Denis Waitley
