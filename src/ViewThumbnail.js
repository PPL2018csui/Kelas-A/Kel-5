import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { TouchableOpacity, View, StyleSheet, Image } from 'react-native';
import { setModalVisible } from '../actions/meta';

class ViewThumbnail extends Component {
  constructor(props) {
    super(props);
    this.showModal = this.showModal.bind(this);
    this.state = {
      src: ''
    };
  }

  componentWillMount() {
    this.setState({ src: this.props.src });
  }

  showModal() {
    this.props.setModalVisible({
      uri: this.state.src
      // data dummy for testing
    });
  }

  render() {
    return (
      <View>
        <TouchableOpacity onPress={this.showModal}>
          <Image style={styles.thumbnail} source={this.props.src} />
        </TouchableOpacity>
      </View>
    );
  }
}

ViewThumbnail.propTypes = {
  src: PropTypes.string,
  setModalVisible: PropTypes.func.isRequired
};

ViewThumbnail.defaultProps = {
  src: ''
};

const styles = StyleSheet.create({
  thumbnail: {
    width: '250px',
    height: '200px'
  },
  thumbnailBig: {
    width: '500px',
    height: '400px'
  }
});

export default connect(null, { setModalVisible })(ViewThumbnail);
