import reducer from '../../reducers/user';
import {
  GROUP_ID_LOADED,
  UNREAD_LOADED
} from '../../types';

describe('User Reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      activeId: '',
      groupIds: [],
      unreads: {}
    });
  });

  it('should handle GROUP_ID_LOADED', () => {
    expect(reducer(
      {
        activeId: 'abcd',
        groupIds: [],
        unreads: {}
      },
      { type: GROUP_ID_LOADED, groupId: ['abcd', 'defgh'] }
    )).toEqual({
      activeId: 'abcd',
      groupIds: ['abcd', 'defgh'],
      unreads: {}
    });
  });

  it('should handle UNREAD_LOADED', () => {
    const state = {
      activeId: 'huhuhu',
      groupId: ['hihi', 'hehe'],
      unreads: {}
    };

    const action = {
      type: UNREAD_LOADED,
      unreads: { hihi: 10, hehe: 11 }
    };

    expect(reducer(state, action)).toEqual({
      activeId: 'huhuhu',
      groupId: ['hihi', 'hehe'],
      unreads: { hihi: 10, hehe: 11 }
    });
  });
});
