const firebase = require('firebase');

const config = {
  apiKey: 'AIzaSyDKBy2HqPHsTTk9lBYo-w34AGneHjywV0c',
  authDomain: 'deco-api.firebaseapp.com',
  databaseURL: 'https://deco-api.firebaseio.com',
  projectId: 'deco-api',
  storageBucket: 'deco-api.appspot.com',
  messagingSenderId: '70425507377'
};

firebase.initializeApp(config);

module.exports = firebase;
