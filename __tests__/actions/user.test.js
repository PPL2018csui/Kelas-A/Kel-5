import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import proxyquire from 'proxyquire';
import firebasemock from 'firebase-mock';
import * as types from '../../types';

// Mock Redux
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

// Mock Firebase
const mockdatabase = new firebasemock.MockFirebase();
const mocksdk = firebasemock.MockFirebaseSdk(path => (path ? mockdatabase.child(path) : mockdatabase));
const mockapp = mocksdk.initializeApp();

let firebase;
let dbref;
let actions;
let userRef;

describe('User actions', () => {
  beforeAll(() => {
    mockdatabase.autoFlush();
    actions = proxyquire('../../actions/user', {
      firebase: () => mockapp
    });
    jest.mock('../../firebase', () => mocksdk);
    firebase = require('../../firebase');
    dbref = firebase.database().ref();

    userRef = dbref.child('user');
    const groupRef = dbref.child('group');

    groupRef.set({
      reignofterror: {
        group_name: 'French Revolution',
        last_message: {
          message: {
            content: '...',
            type: 'text'
          },
          timestamp: 150333111222,
          user_id: 'louis16'
        },
        members: {
          0: 'louis16',
          1: 'robespierre'
        }
      },
      revolutionist: {
        group_name: 'French Revolutionist',
        last_message: {
          message: {
            content: '...',
            type: 'text'
          },
          timestamp: 150333111222,
          user_id: 'louis16'
        },
        members: {
          0: 'robespierre',
          1: 'louis16'
        }
      }
    });

    userRef.set({
      louis16: {
        name: 'Louis XVI of France',
        groups: {
          0: 'reignofterror',
          1: 'revolutionist'
        },
        img_url: 'nangid',
      },
      robespierre: {
        name: 'Maximilien Francois Marie Isidore de Robespierre',
        groups: {
          0: 'reignofterror',
          1: 'revolutionist'
        },
        img_url: 'h3h3',
        unread_chats: {
          reignofterror: {
            0: 0,
            1: 1
          },
          revolutionist: {
            0: 10,
            1: 11
          }
        }
      }
    });
  });

  const uid = 'robespierre';

  it('should dispatch get group IDs', () => {
    const groupId = ['hahahay'];
    const expectedAction = {
      type: types.GROUP_ID_LOADED,
      groupId
    };
    expect(actions.groupIdLoaded(groupId)).toEqual(expectedAction);
  });

  it('should dispatch unread load', () => {
    const val = { versailles: 10, bastille: 11 };
    const expected = {
      type: types.UNREAD_LOADED,
      unreads: val
    };

    expect(actions.unreadLoaded(val)).toEqual(expected);
  });

  describe('on function getUserById', () => {
    it('should return user object', () => {
      const expectedResult = {
        name: 'Maximilien Francois Marie Isidore de Robespierre',
        groups: {
          0: 'reignofterror',
          1: 'revolutionist'
        },
        img_url: 'h3h3',
        unread_chats: {
          reignofterror: {
            0: 0,
            1: 1
          },
          revolutionist: {
            0: 10,
            1: 11
          }
        }
      };
      const store = mockStore({});

      store.dispatch(actions.getUserById(uid)).then((user) => {
        expect(user).toEqual(expectedResult);
      });
    });
  });

  it('should load group objects of a user', () => {
    const expectedActions = [
      {
        type: types.GROUP_ID_LOADED,
        groupId: ['reignofterror', 'revolutionist']
      },
      {
        type: types.GROUP_LOADED,
        groups: {
          reignofterror: {
            group_name: 'French Revolution',
            last_message: {
              message: {
                content: '...',
                type: 'text'
              },
              timestamp: 150333111222,
              user_id: 'louis16'
            },
            members: {
              0: 'louis16',
              1: 'robespierre'
            }
          },
          revolutionist: {
            group_name: 'French Revolutionist',
            last_message: {
              message: {
                content: '...',
                type: 'text'
              },
              timestamp: 150333111222,
              user_id: 'louis16'
            },
            members: {
              0: 'robespierre',
              1: 'louis16'
            }
          }
        }
      }
    ];
    const store = mockStore({});

    store.dispatch(actions.getGroupIds(uid));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should handle getUnread', () => {
    const expected = [{
      type: types.UNREAD_LOADED,
      unreads: {
        reignofterror: {
          0: 0,
          1: 1
        },
        revolutionist: {
          0: 10,
          1: 11
        }
      }
    }];

    const store = mockStore({});
    store.dispatch(actions.getUnread(uid));
    expect(store.getActions()).toEqual(expected);
  });

  it('should push notif to user on function pushNotif()', () => {
    const memberArray = ['louis16'];
    const chatIndex = '23';
    const groupId = 'reignofterror';
    const expectedUnread = { reignofterror: ['23'] };
    const store = mockStore({});

    store.dispatch(actions.pushNotif(memberArray, chatIndex, groupId));
    dbref.child('user/louis16/unread_chats').on('value', (snap) => {
      expect(snap.val()).toEqual(expectedUnread);
    });
  });

  it('should handle clearUnread', () => {
    const groupId = 'reignofterror';
    const expected = [{
      type: types.UNREAD_LOADED,
      unreads: {
        reignofterror: {
          0: 0,
          1: 1
        },
        revolutionist: {
          0: 10,
          1: 11
        }
      }
    }];
    const store = mockStore({});
    actions.clearUnread(uid, groupId);
    store.dispatch(actions.getUnread(uid));
    expect(store.getActions()).toEqual(expected);
  });

  afterAll(() => {});
});
