import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView, View, StyleSheet, Dimensions } from 'react-native';
import ConversationItem from './ConversationItem';
import { selectGroup, getGroups } from '../actions/group';
import { getUnread } from '../actions/user';
import { connect } from 'react-redux';

const deviceHeight = Dimensions.get('window').height;
class ConversationItemPanel extends React.Component {
  componentWillMount() {
    this.props.getGroups(this.props.userId);
    this.props.getUnread(this.props.userId);
  }

  render() {
    const { groups } = this.props;
    const sortedGroup = Object.keys(groups).sort((a, b) =>
      groups[b].last_message.timestamp - groups[a].last_message.timestamp);
    const groupList = sortedGroup.map(groupId => (
      <ConversationItem
        key={groupId}
        groupId={groupId}
        groupName={groups[groupId].group_name}
        select={this.props.selectGroup}
        groupImage={`../assets/${groups[groupId].img_url}`}
        type={groups[groupId].last_message.message.type}
      >
        {groups[groupId].last_message.message.content}
      </ConversationItem>
    ));

    return (
      <View style={styles.ConversationItemPanel}>
        <ScrollView>{groupList}</ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    groups: state.group.groupList,
    userId: state.user.activeId,
    userGroups: state.user.groupIds
  };
}

ConversationItemPanel.propTypes = {
  getGroups: PropTypes.func.isRequired,
  selectGroup: PropTypes.func.isRequired,
  getUnread: PropTypes.func.isRequired,
  groups: PropTypes.shape().isRequired,
  userId: PropTypes.string.isRequired
};

const styles = StyleSheet.create({
  ConversationItemPanel: {
    flex: 1,
    justifyContent: 'center',
    height: deviceHeight * 0.9,
    borderRightWidth: 0.5,
    borderColor: '#d6d7da'
  }
});

export default connect(mapStateToProps, {
  selectGroup,
  getGroups,
  getUnread
})(ConversationItemPanel);
