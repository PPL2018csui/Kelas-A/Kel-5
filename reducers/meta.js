import { TOGGLE_MEMBER, CLOSE_MEMBER_TOGGLE, THUMBNAIL_SET, CLOSE_THUMBNAIL } from '../types.js';

export default function meta(state = { memberlist_open: false, thumbnail: '' }, action) {
  switch (action.type) {
  case TOGGLE_MEMBER:
    return { ...state, memberlist_open: !state.memberlist_open };
  case CLOSE_MEMBER_TOGGLE:
    return { ...state, memberlist_open: false };
  case THUMBNAIL_SET:
    return { ...state, thumbnail: action.pic };
  case CLOSE_THUMBNAIL:
    return { ...state, thumbnail: '' };
  default:
    return state;
  }
}
