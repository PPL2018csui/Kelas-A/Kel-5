import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  Dimensions,
  View,
  TouchableOpacity,
  TextInput
} from 'react-native';
import { connect } from 'react-redux';
import { sendChat } from '../actions/chat';
import UploadButton from './UploadButton';

const deviceHeight = Dimensions.get('window').height;

class InputTextPanel extends Component {
  constructor() {
    super();
    this.state = {
      data: {
        chat: ''
      },
      error: ''
    };
  }

  onSend() {
    if (this.state.data.chat.length > 1000) {
      this.state.error = 'Text limit is reached';
      return false;
    } else if (this.state.data.chat.length === 0) {
      this.state.error = 'Text can not be emptied';
      return false;
    } else if (this.props.groupId === '') {
      this.state.error = 'Please select a group';
      return false;
    }
    return true;
  }

  render() {
    const { data } = this.state;
    return (
      <View style={styles.contentContainer}>
        <UploadButton />
        <View style={styles.content1}>
          <Text style={styles.textStyleWarn}>{this.state.error}</Text>
          <TextInput
            style={styles.input}
            placeholder="Write Anything"
            maxLength={1001}
            multiline={true}
            value={data.chat}
            name="chat"
            onChangeText={text =>
              this.setState({ data: { ...data, chat: text }, error: '' })
            }
          />
        </View>
        <View style={styles.content2}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              const isReady = this.onSend();
              if (isReady) {
                this.props.sendChat(
                  this.state.data.chat,
                  this.props.groupId,
                  this.props.userId
                );
                this.setState({
                  data: { ...data, chat: '' },
                  error: ''
                });
              } else {
                this.setState({
                  data: { ...data, chat: this.state.data.chat },
                  error: this.state.error
                });
              }
            }}
          >
            <Text style={styles.white}>Kirim</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

InputTextPanel.propTypes = {
  sendChat: PropTypes.func.isRequired,
  groupId: PropTypes.string.isRequired,
  userId: PropTypes.string.isRequired
};

function mapStateToProps(state) {
  return {
    groupId: state.group.selected,
    userId: state.user.activeId
  };
}

const styles = StyleSheet.create({
  contentContainer: {
    height: deviceHeight * 0.12,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#e5e5e5',
    padding: 10
  },
  content1: {
    flex: 12,
    flexDirection: 'column'
  },
  content2: {},
  button: {
    borderRadius: 5,
    padding: 10,
    backgroundColor: '#02B0B9',
    alignSelf: 'flex-end',
    margin: 5
  },
  input: {
    flex: 1,
    backgroundColor: 'white',
    padding: 10,
    margin: 0
  },
  textStyleWarn: {
    justifyContent: 'center',
    paddingLeft: 5,
    backgroundColor: 'grey',
    color: 'yellow',
    fontWeight: 'bold'
  },
  white: {
    color: 'white'
  }
});

export default connect(mapStateToProps, { sendChat })(InputTextPanel);
