import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Modal,
  Image,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ChatPanelFirstView from './ChatPanelFirstView';
import ConversationListPanel from './ConversationListPanel';
import LoginPage from './LoginPage';
import ChatPanel from './ChatPanel';
import { closeThumbnail } from '../actions/meta';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

class ContainerApp extends Component {
  render() {
    return (
      <View>
        {this.props.userId ? (
          <View>
            <View style={styles.containerApp}>
              <ConversationListPanel />
              {this.props.groupSelected ? (
                <ChatPanel />
              ) : (
                <ChatPanelFirstView />
              )}
            </View>
            {this.props.thumbnail !== '' && (
              <Modal
                animationType="slide"
                transparent={false}
                visible={true}
                onRequestClose={() => {
                  alert('Modal has been closed.');
                }}
                style={{
                  width: '100vw',
                  height: '100vh',
                  position: 'absolute',
                  border: 0
                }}
              >
                <TouchableOpacity
                  style={{
                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '100%'
                  }}
                  onPress={this.props.closeThumbnail}
                >
                  <View>
                    <Image
                      style={styles.thumbnailBig}
                      source={this.props.thumbnail}
                      resizeMode="contain"
                    />
                  </View>
                </TouchableOpacity>
              </Modal>
            )}
          </View>
        ) : (
          <LoginPage />
        )}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    groupSelected: !!state.group.selected,
    thumbnail: state.meta.thumbnail,
    userId: !!state.user.activeId
  };
}

ContainerApp.propTypes = {
  groupSelected: PropTypes.bool.isRequired,
  thumbnail: PropTypes.string,
  closeThumbnail: PropTypes.func.isRequired,
  userId: PropTypes.bool.isRequired
};

ContainerApp.defaultProps = {
  thumbnail: ''
};

const styles = StyleSheet.create({
  containerApp: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#f1f3f2',
    flexDirection: 'row'
  },
  thumbnailBig: {
    width: `${deviceWidth / 2}px`,
    height: `${deviceHeight / 2}px`
  },
  imageView: {
    width: '100%',
    height: '100%',
    position: 'absolute'
  },
  buttonClose: {
    backgroundColor: '#02b0b9',
    color: 'white',
    width: 100,
    height: 25,
    borderRadius: 10,
    textAlign: 'center',
    marginTop: 'auto',
    marginBottom: 'auto',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  biggerView: {
    color: 'black',
    position: 'absolute'
  }
});

export default connect(mapStateToProps, { closeThumbnail })(ContainerApp);
