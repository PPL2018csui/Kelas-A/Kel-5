import reducer from '../../reducers/group';
import {
  GROUP_LOADED,
  GROUP_SELECTED,
  MEMBER_LIST_ID_LOADED
} from '../../types';

describe('Group Reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      groupList: {},
      selected: '',
      memberIds: []
    });
  });

  it('should handle GROUP_LOADED', () => {
    expect(reducer(
      {
        groupList: {},
        selected: '',
        memberIds: []
      },
      { type: GROUP_LOADED, groups: { name: 'abcd', key: 'defgh' } }
    )).toEqual({
      groupList: { name: 'abcd', key: 'defgh' },
      selected: '',
      memberIds: []
    });
  });

  it('should handle GROUP_SELECTED', () => {
    expect(reducer(
      {
        groupList: {},
        selected: '',
        memberIds: []
      },
      { type: GROUP_SELECTED, groupId: 'defgh' }
    )).toEqual({
      groupList: {},
      selected: 'defgh',
      memberIds: []
    });
  });

  it('should handle MEMBER_LIST_ID_LOADED', () => {
    expect(reducer(
      {
        groupList: {},
        selected: '',
        memberIds: []
      },
      { type: MEMBER_LIST_ID_LOADED, memberList: ['abcd', 'defgh'] }
    )).toEqual({
      groupList: {},
      selected: '',
      memberIds: ['abcd', 'defgh']
    });
  });
});
