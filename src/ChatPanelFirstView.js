import React from 'react';
import { View, StyleSheet, Image, Dimensions } from 'react-native';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default class ChatPanelFirstView extends React.Component {
  render() {
    const pic = {
      uri: '../assets/panel_first.png'
    };
    return (
      <View style={styles.container}>
        <View style={styles.firstViewPanel}>
          <Image style={styles.firstViewPic} source={pic} resizeMode="contain" />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 5,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center'
  },
  firstViewPanel: {
    flex: 1,
    alignItems: 'center',
    top: deviceHeight / 3.5,
    left: 0,
    right: 0
  },
  firstViewPic: {
    width: `${deviceWidth / 6}px`,
    height: `${deviceWidth / 6}px`,
    position: 'absolute'
  }
});
