import 'react-native';
import React from 'react';
import configureStore from 'redux-mock-store';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ConversationListPanel from '../src/ConversationListPanel';

Enzyme.configure({ adapter: new Adapter() });

// create any initial state needed
const initialState = {
  user: {
    activeId: 'blackprince'
  }
};
// here it is possible to pass in any middleware if needed into //configureStore
const mockStore = configureStore();
let store;
beforeEach(() => {
  // creates the store with any initial state or middleware needed
  store = mockStore(initialState);
});

it('should render the class', () => {
  const panel = shallow(<ConversationListPanel store={store} />);
  expect(panel);
});
