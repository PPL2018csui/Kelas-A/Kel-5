import { CHAT_SENT, CHAT_LOADED } from '../types';
import firebase from '../firebase';
import { pushNotif, clearUnread } from './user';

export const chatSent = () => ({
  type: CHAT_SENT
});

export const chatLoaded = chats => ({
  type: CHAT_LOADED,
  chats
});

export const sendChat = (chat, groupId, userId) => (dispatch) => {
  let chatLength = 0;
  const chatRef = firebase
    .database()
    .ref()
    .child(`group/${groupId}/chats`);
  const date = new Date().getTime();

  // Set array key
  chatRef.once('value', (snap) => {
    if (snap.val() !== null) {
      chatLength = snap.val().length;
    }
  });

  const chatContent = {
    message: {
      content: chat,
      type: 'text'
    },
    timestamp: date,
    user_id: userId
  };

  // Insert chat to array
  firebase
    .database()
    .ref()
    .child(`group/${groupId}/chats/${chatLength}`)
    .set(chatContent);

  // Set last message of the group
  firebase
    .database()
    .ref()
    .child(`group/${groupId}/last_message`)
    .set(chatContent);

  // Set notif to group members
  firebase
    .database()
    .ref()
    .child(`group/${groupId}/members`)
    .once('value', (snap) => {
      const memberArray = snap.val().filter(item => item !== userId);
      dispatch(pushNotif(memberArray, chatLength, groupId));
    });

  dispatch(chatSent());
};

// TODO: Define this need redux or not
export const setReadBy = (userId, groupId) => () => {
  firebase
    .database()
    .ref()
    .child(`user/${userId}/unread_chats/${groupId}`)
    .once('value', (snap) => {
      const chatIds = snap.val();

      if (chatIds !== null) {
        chatIds.forEach((chatId) => {
          firebase
            .database()
            .ref()
            .child(`group/${groupId}/chats/${chatId}/read_by`)
            .push(userId);
        });
      }
    });
};

export const getChats = (userId, groupId) => (dispatch) => {
  const chatRef = firebase
    .database()
    .ref()
    .child(`group/${groupId}/chats`);

  chatRef.on('value', (snap) => {
    const chatArray = snap.val();
    const chatWithUser = [];

    if (chatArray !== null) {
      for (let i = 0, len = chatArray.length; i < len; i += 1) {
        if (chatArray[i] !== undefined) {
          let user;
          firebase
            .database()
            .ref()
            .child(`user/${chatArray[i].user_id}`)
            .on('value', (data) => {
              user = data.val();
              chatWithUser.push({ ...chatArray[i], user: user });
            });
        }
      }
    }

    dispatch(chatLoaded(chatWithUser));
    dispatch(clearUnread(userId, groupId));
  });
};
