import 'react-native';
import React from 'react';
import configureStore from 'redux-mock-store';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ChatBubbleLeft from '../src/ChatBubbleLeft';

Enzyme.configure({ adapter: new Adapter() });

// create any initial state needed
const initialState = {
  group: {
    groupList: {
      L8ghPbwEg8CFA11nORg: {
        groupName: 'Group 1',
        last_message: {
          message: {
            content: 'Anjayy',
            type: 'text'
          },
          timestamp: 1523252143018,
          user_id: 'jeannedarc'
        }
      },
      L8hoCPbTa3Ijsj1v1g6: {
        groupName: 'Group 2',
        last_message: {
          message: {
            content: 'Weleh',
            type: 'text'
          },
          timestamp: 1523252145018,
          user_id: 'jeannedarc'
        }
      }
    }
  }
};
// here it is possible to pass in any middleware if needed into //configureStore
const mockStore = configureStore();
let store;
beforeEach(() => {
  // creates the store with any initial state or middleware needed
  store = mockStore(initialState);
});

describe('<ChatBubbleLeft />', () => {
  it('should content the class', () => {
    const panel = shallow(<ChatBubbleLeft store={store} />);
    expect(panel);
  });
});
