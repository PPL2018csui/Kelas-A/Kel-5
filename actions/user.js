import { getGroupByIds } from './group';
import {
  GROUP_ID_LOADED,
  UNREAD_LOADED,
  UNREAD_CLEARED,
  USER_ID_SET
} from '../types';
import firebase from '../firebase';

export const groupIdLoaded = groupId => ({
  type: GROUP_ID_LOADED,
  groupId
});

export const unreadLoaded = unreads => ({
  type: UNREAD_LOADED,
  unreads
});

export const unreadCleared = groupId => ({
  type: UNREAD_CLEARED,
  groupId
});

export const userIdSet = userId => ({
  type: USER_ID_SET,
  userId
});

export const getUnread = userId => (dispatch) => {
  const unreadRef = firebase
    .database()
    .ref()
    .child(`user/${userId}/unread_chats`);

  unreadRef.on('value', (snap) => {
    dispatch(unreadLoaded(snap.val()));
  });
};

export const clearUnread = (userId, groupId) => () => {
  firebase
    .database()
    .ref()
    .child(`user/${userId}/unread_chats/${groupId}`)
    .remove();
};

export const getGroupIds = userId => (dispatch) => {
  const groupRef = firebase
    .database()
    .ref()
    .child(`user/${userId}/groups`);

  groupRef.on('value', (snap) => {
    dispatch(groupIdLoaded(snap.val()));
    dispatch(getGroupByIds(snap.val()));
  });
};

export const getMemberByIds = memberIds => () =>
  new Promise((resolve) => {
    const memberArray = [];
    const userRef = firebase
      .database()
      .ref()
      .child('user');

    memberIds.forEach((memberId) => {
      userRef.child(memberId).on('value', (snap) => {
        memberArray.push({ ...snap.val(), id: memberId });
        if (memberArray.length === memberIds.length) {
          resolve(memberArray);
        }
      });
    });
  });

export const pushNotif = (memberIds, chatIndex, groupId) => () => {
  memberIds.forEach((memberId) => {
    const userUnread = firebase
      .database()
      .ref()
      .child(`user/${memberId}/unread_chats/${groupId}`);

    userUnread.once('value', (snap) => {
      let unread;

      if (snap.val() == null) {
        unread = [];
      } else {
        unread = snap.val();
      }

      unread.push(chatIndex);
      userUnread.set(unread);
    });
  });
};

export const getUserById = userId => () =>
  new Promise((resolve) => {
    const userRef = firebase
      .database()
      .ref()
      .child(`user/${userId}`);

    userRef.on('value', (snap) => {
      resolve(snap.val());
    });
  });
