import { GROUP_LOADED, GROUP_SELECTED, MEMBER_LIST_ID_LOADED } from '../types';

export default function group(
  state = { groupList: {}, selected: '', memberIds: [] },
  action
) {
  switch (action.type) {
  case GROUP_LOADED:
    return { ...state, groupList: action.groups };
  case GROUP_SELECTED:
    return { ...state, selected: action.groupId };
  case MEMBER_LIST_ID_LOADED:
    return { ...state, memberIds: action.memberList };
  default:
    return state;
  }
}
