import 'react-native';
import React from 'react';
import configureStore from 'redux-mock-store';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ViewThumbnail from '../src/ViewThumbnail';

Enzyme.configure({ adapter: new Adapter() });

const initialState = {
  group: {
    groupList: {
      abcd: {}
    }
  },
  meta: {
    thumbnail: ''
  },
  user: {
    activeId: '123'
  }
};
// here it is possible to pass in any middleware if needed into //configureStore
const mockStore = configureStore();
let store;
beforeEach(() => {
  // creates the store with any initial state or middleware needed
  store = mockStore(initialState);
});

describe('<View Thumbnail />', () => {
  it('should content the class', () => {
    const panel = shallow(<ViewThumbnail store={store} />);
    expect(panel);
  });
});
