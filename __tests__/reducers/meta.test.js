import reducer from '../../reducers/meta';
import {
  TOGGLE_MEMBER,
  CLOSE_MEMBER_TOGGLE,
  THUMBNAIL_SET,
  CLOSE_THUMBNAIL
} from '../../types';

describe('Meta Reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({ memberlist_open: false, thumbnail: '' });
  });

  it('should handle TOGGLE_MEMBER', () => {
    const action = { type: TOGGLE_MEMBER };
    const state1 = {
      memberlist_open: true,
      thumbnail: 'h3h3'
    };

    const state2 = {
      memberlist_open: false,
      thumbnail: 'huhu'
    };

    expect(reducer(state1, action)).toEqual({
      memberlist_open: false,
      thumbnail: 'h3h3'
    });

    expect(reducer(state2, action)).toEqual({
      memberlist_open: true,
      thumbnail: 'huhu'
    });
  });

  it('should handle CLOSE_MEMBER_TOGGLE', () => {
    const action = { type: CLOSE_MEMBER_TOGGLE };
    expect(reducer({
      memberlist_open: true,
      thumbnail: 'hoho'
    }, action)).toEqual({
      memberlist_open: false,
      thumbnail: 'hoho'
    });

    expect(reducer({
      memberlist_open: false,
      thumbnail: 'h3h3'
    }, action)).toEqual({
      memberlist_open: false,
      thumbnail: 'h3h3'
    });
  });

  it('should handle THUMBNAIL_SET', () => {
    const action = {
      type: THUMBNAIL_SET,
      pic: 'huehue'
    };

    expect(reducer({
      memberlist_open: 'whatever',
      thumbnail: 'hihi'
    }, action)).toEqual({
      memberlist_open: 'whatever',
      thumbnail: 'huehue'
    });
  });

  it('should handle CLOSE_THUMBNAIL', () => {
    const action = { type: CLOSE_THUMBNAIL };
    expect(reducer({
      memberlist_open: 'whatever',
      thumbnail: 'terserah'
    }, action)).toEqual({
      memberlist_open: 'whatever',
      thumbnail: ''
    });
  });
});
