import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import proxyquire from 'proxyquire';
import firebasemock from 'firebase-mock';
import * as types from '../../types';

// Mock Redux
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

// Mock Firebase
const mockdatabase = new firebasemock.MockFirebase();
const mocksdk = firebasemock.MockFirebaseSdk(path => (path ? mockdatabase.child(path) : mockdatabase));
const mockapp = mocksdk.initializeApp();

let firebase;
let userMock;
let actions;

describe('Chat Actions', () => {
  beforeAll(() => {
    mockdatabase.autoFlush();
    actions = proxyquire('../../actions/chat', {
      firebase: () => mockapp
    });
    jest.mock('../../firebase', () => mocksdk);
    firebase = require('../../firebase');
    userMock = firebase.database().ref();

    userMock.set({
      user: {
        123: {
          groups: ['L8ghPbwEg8CFA11nORg', 'L8hoCPbTa3Ijsj1v1g6'],
          img_url: 'hehe1111',
          name: 'Edward the Black Prince'
        }
      },
      group: {
        L8ghPbwEg8CFA11nORg: {
          group_name: 'Group 1',
          last_message: 'Hehehe',
          chats: [
            {
              message: {
                content: 'Asdf',
                type: 'text'
              },
              timestamp: 1523251984407,
              user_id: '123'
            },
            {
              message: {
                content: 'Huehue',
                type: 'text'
              },
              timestamp: 1523252984407,
              user_id: '123'
            }
          ]
        },
        L8hoCPbTa3Ijsj1v1g6: {
          group_name: 'Group 2',
          last_message: 'wkwkwkwk',
          chats: []
        }
      }
    });
  });

  it('should dispatch when chat sent', () => {
    const expectedAction = {
      type: types.CHAT_SENT
    };
    expect(actions.chatSent()).toEqual(expectedAction);
  });

  it('should dispatch when chat loaded', () => {
    const chats = { id: 0, chat: 'hahahay' };
    const expectedAction = {
      type: types.CHAT_LOADED,
      chats
    };
    expect(actions.chatLoaded(chats)).toEqual(expectedAction);
  });

  it('should return empty array if chats is empty', () => {
    const groupId = 'L8hoCPbTa3Ijsj1v1g6';
    const chats = [];
    const expectedActions = [
      {
        type: types.CHAT_LOADED,
        chats
      }
    ];
    const store = mockStore({});

    store.dispatch(actions.getChats(groupId));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should send chat on function sendChat()', () => {
    const chat = 'hahahay';
    const groupId = 'L8ghPbwEg8CFA11nORg';
    const userId = 123;
    const expectedActions = [
      {
        type: types.CHAT_SENT
      }
    ];
    const store = mockStore({});

    store.dispatch(actions.sendChat(chat, groupId, userId));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should send chat on empty group', () => {
    const chat = 'hahahay';
    const groupId = 'L8hoCPbTa3Ijsj1v1g6';
    const userId = 123;
    const expectedActions = [
      {
        type: types.CHAT_SENT
      }
    ];
    const store = mockStore({});

    store.dispatch(actions.sendChat(chat, groupId, userId));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
