import React from 'react';
import { View, Image } from 'react-native';
import UploadButton from '../src/UploadButton';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { expect } from 'chai';

Enzyme.configure({ adapter: new Adapter() });

const initialState = {
  group: {
    selected: ''
  },
  user: {
    activeId: ''
  },
};

const mockStore = configureStore();
let store;
beforeEach(() => {
  // creates the store with any initial state or middleware needed
  store = mockStore(initialState);
});

describe('<UploadButton>', () => {
  test('This should be like', () => {
    const component = shallow(<UploadButton store={store} />);
    expect(component);
  });

  test('View should be like', () => {
    const view = renderer.create(<UploadButton store={store} />);
    expect(view).to.include(View);
  });

  test('Image should be like', () => {
    const image = renderer.create(<UploadButton store={store} />);
    expect(image).to.include(Image);
  });
});
