import { CHAT_SENT, CHAT_LOADED } from '../types';

export default function chat(state = { chatList: [] }, action) {
  switch (action.type) {
  case CHAT_SENT:
    return { ...state };
  case CHAT_LOADED:
    return { ...state, chatList: action.chats };
  default:
    return state;
  }
}
