import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import Header from './Header';
import { switchToggle } from '../actions/meta';
import ScrollbarPanel from './ScrollbarPanel';
import InputTextPanel from './InputTextPanel';
import MemberListPanel from './MemberListPanel';

const deviceWidth = Dimensions.get('window').width;

class ChatPanel extends React.Component {
  render() {
    const { memberListOpen } = this.props;

    const panel = memberListOpen ? (
      <MemberListPanel />
    ) : (
      <View>
        <ScrollbarPanel
          chats={this.props.group !== undefined ? this.props.group.chats : []}
        />
        <InputTextPanel />
      </View>
    );
    return (
      <View style={styles.chatPanel}>
        <Header>
          {this.props.group !== undefined ? (
            <TouchableOpacity onPress={this.props.switchToggle}>
              <View style={styles.header}>
                <Text>{this.props.group.group_name}</Text>
                <Text style={styles.subTitle}>click here to see member</Text>
              </View>
            </TouchableOpacity>
          ) : (
            ''
          )}
        </Header>
        {panel}
      </View>
    );
  }
}

ChatPanel.propTypes = {
  group: PropTypes.shape({
    group_name: PropTypes.string,
    chats: PropTypes.array
  }),
  memberListOpen: PropTypes.bool.isRequired,
  switchToggle: PropTypes.func.isRequired
};

ChatPanel.defaultProps = {
  group: {
    group_name: '',
    chats: []
  }
};

function mapStateToProps(state) {
  return {
    group: state.group.groupList[state.group.selected],
    memberListOpen: state.meta.memberlist_open
  };
}

const styles = StyleSheet.create({
  chatPanel: {
    flex: 7,
    flexDirection: 'column'
  },
  header: {
    width: deviceWidth * 0.69,
    paddingLeft: 10
  },
  subTitle: {
    color: '#ffffff',
    fontSize: 15,
    fontWeight: 'normal'
  }
});

export default connect(mapStateToProps, { switchToggle })(ChatPanel);
