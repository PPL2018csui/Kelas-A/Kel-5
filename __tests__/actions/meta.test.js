import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import * as actions from '../../actions/meta';
import * as types from '../../types';

// Mock Redux
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Meta Actions', () => {
  it('should dispatch toggle switching on function switchToggle()', () => {
    const expectedAction = [
      {
        type: types.TOGGLE_MEMBER
      }
    ];

    const store = mockStore({});

    store.dispatch(actions.switchToggle());
    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch switch toggle member', () => {
    const expectedAction = {
      type: types.TOGGLE_MEMBER
    };
    expect(actions.toggleMember()).toEqual(expectedAction);
  });

  it('should dispatch close toggle member', () => {
    const expectedAction = {
      type: types.CLOSE_MEMBER_TOGGLE
    };
    expect(actions.closeMemberToggle()).toEqual(expectedAction);
  });

  it('should dispatch thumbnail set with store', () => {
    const pic = 'huehue';
    const expectedAction = [{
      type: types.THUMBNAIL_SET,
      pic: pic
    }];

    const store = mockStore({});
    store.dispatch(actions.setModalVisible(pic));
    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch thumbnail set', () => {
    const pic = 'huehue';
    const expectedAction = {
      type: types.THUMBNAIL_SET,
      pic: pic
    };

    expect(actions.setThumbnail(pic)).toEqual(expectedAction);
  });

  it('should dispatch close thumbnail', () => {
    const expectedAction = { type: types.CLOSE_THUMBNAIL };
    expect(actions.closeThumbnail()).toEqual(expectedAction);
  });
});
