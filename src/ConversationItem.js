import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Image,
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity
} from 'react-native';

const deviceHeight = Dimensions.get('window').height;
class ConversationItem extends React.Component {
  render() {
    const { unreads, groupId, userId } = this.props;

    return (
      <TouchableOpacity
        style={styles.ConversationItemPanel}
        onPress={() => this.props.select(userId, groupId)}
      >
        <View style={styles.container}>
          <View style={styles.container1}>
            <Image
              style={styles.pictureImg}
              source={{ uri: this.props.groupImage }}
              resizeMode="contain"
            />
          </View>
          <View style={styles.container2}>
            <Text style={styles.font1} numberOfLines={1} ellipsizeMode="tail">
              {this.props.groupName}
            </Text>
            <Text style={styles.font2} numberOfLines={1} ellipsizeMode="tail">
              {this.props.type === 'text'
                ? this.props.children
                : this.props.children.split('-')[1]}
            </Text>
          </View>
          <View style={styles.container3}>
            {!!unreads &&
              !!unreads[groupId] && (
              <View style={styles.notifPanel}>
                <View style={styles.notifText}>
                  <Text style={styles.white}>{unreads[groupId].length}</Text>
                </View>
              </View>
            )}
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

ConversationItem.propTypes = {
  select: PropTypes.func,
  groupName: PropTypes.string,
  children: PropTypes.string,
  groupId: PropTypes.string,
  userId: PropTypes.string,
  unreads: PropTypes.object,
  groupImage: PropTypes.string,
  type: PropTypes.string
};

ConversationItem.defaultProps = {
  groupName: '',
  children: '',
  groupId: '',
  userId: '',
  select: () => {},
  unreads: {},
  groupImage: 'https://i.imgur.com/0krCEUb.png',
  type: ''
};

function mapStateToProps(state) {
  return {
    unreads: state.user.unreads,
    userId: state.user.activeId
  };
}

const styles = StyleSheet.create({
  ConversationItemPanel: {
    flex: 1,
    height: deviceHeight * 0.18,
    padding: 10
  },
  font1: {
    fontSize: 28,
    padding: 10
  },
  font2: {
    fontSize: 20,
    padding: 10
  },
  name: {
    fontSize: 18,
    flex: 5
  },
  container: {
    flexDirection: 'row',
    flex: 1
  },
  container1: {
    paddingTop: 10,
    paddingBottom: 10
  },
  container2: {
    flex: 1,
    flexDirection: 'column',
    borderBottomWidth: 0.5,
    borderColor: '#d6d7da'
  },
  container3: {
    paddingTop: 50,
    paddingBottom: 50
  },
  notifPanel: {
    width: 30,
    height: 20,
    borderRadius: 27.5,
    backgroundColor: '#02B0B9',
    alignSelf: 'flex-end'
  },
  notifText: {
    alignSelf: 'center',
    margin: 1
  },
  pictureImg: {
    width: deviceHeight / 7,
    height: deviceHeight / 7
  },
  white: {
    color: 'white'
  }
});

export default connect(mapStateToProps)(ConversationItem);
