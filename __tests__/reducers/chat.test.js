import reducer from '../../reducers/chat';
import { CHAT_SENT, CHAT_LOADED } from '../../types';

describe('Chat Reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({ chatList: [] });
  });

  it('should handle CHAT_LOADED', () => {
    expect(reducer({ chatList: [] }, { type: CHAT_LOADED, chats: ['abcd', 'defgh'] })).toEqual({ chatList: ['abcd', 'defgh'] });
  });

  it('should handle CHAT_SENT', () => {
    expect(reducer({ chatList: [] }, { type: CHAT_SENT })).toEqual({
      chatList: []
    });
  });
});
