import 'react-native';
import React from 'react';
import configureStore from 'redux-mock-store';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import MemberListPanel from '../src/MemberListPanel';

Enzyme.configure({ adapter: new Adapter() });

// create any initial state needed
const initialState = {
  group: {
    selected: 'abcde'
  },
  user: {
    activeId: 'jeannedarc'
  }
};
// here it is possible to pass in any middleware if needed into //configureStore
const mockStore = configureStore();
let store;
beforeEach(() => {
  // creates the store with any initial state or middleware needed
  store = mockStore(initialState);
});
describe('<MemberListPanel />', () => {
  it(' should content the view ', () => {
    const viewPanel = shallow(<MemberListPanel store={store} />);
    expect(viewPanel);
  });
});
