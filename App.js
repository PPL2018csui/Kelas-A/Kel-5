/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import ContainerApp from './src/ContainerApp';
import rootReducer from './rootReducer';

export default class App extends Component {
  render() {
    const store = createStore(
      rootReducer,
      composeWithDevTools(applyMiddleware(thunk))
    );

    return (
      <Provider store={store}>
        <ContainerApp />
      </Provider>
    );
  }
}
