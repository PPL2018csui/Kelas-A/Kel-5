import { TOGGLE_MEMBER, CLOSE_MEMBER_TOGGLE, THUMBNAIL_SET, CLOSE_THUMBNAIL } from '../types';

export const toggleMember = () => ({
  type: TOGGLE_MEMBER
});

export const closeMemberToggle = () => ({
  type: CLOSE_MEMBER_TOGGLE
});

export const setThumbnail = pic => ({
  type: THUMBNAIL_SET,
  pic
});

export const closeThumbnail = () => ({
  type: CLOSE_THUMBNAIL
});

export const switchToggle = () => (dispatch) => {
  dispatch(toggleMember());
};

export const setModalVisible = pic => (dispatch) => {
  dispatch(setThumbnail(pic));
};

export default toggleMember;
