import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView, View, StyleSheet, Dimensions, Text } from 'react-native';
import { connect } from 'react-redux';
import { setReadBy, getChats } from '../actions/chat';
import ChatBubbleRight from './ChatBubbleRight';
import ChatBubbleLeft from './ChatBubbleLeft';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
class ScrollbarPanel extends React.Component {
  constructor() {
    super();
    this.state = {
      currentDate: ''
    };
  }

  componentDidUpdate() {
    if (
      this.props.unreads !== null &&
      this.props.unreads[this.props.groupId] !== null
    ) {
      this.props.setReadBy(this.props.userId, this.props.groupId);
      this.props.getChats(this.props.userId, this.props.groupId);
    }
  }

  getTimeFromDate(timestamp) {
    const date = new Date(timestamp);
    const hours = date.getHours();
    const minutes = date.getMinutes();
    return `${this.pad(hours)}:${this.pad(minutes)}`;
  }

  getDateForClustering(timestamp) {
    const date = new Date(timestamp);
    const months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec'
    ];
    const month = months[date.getMonth()];
    const day = date.getDate();
    return `${month} ${day}`;
  }

  showChatClustering(timestamp) {
    const date = `${this.getDateForClustering(timestamp)}`;
    if (this.state.currentDate !== date) {
      this.state.currentDate = date;
      return this.getDateForClustering(timestamp);
    }
  }

  pad(num) {
    return `0${num}`.slice(-2);
  }

  render() {
    const { chats } = this.props;
    const sortedChat = chats.sort((a, b) => a.timestamp - b.timestamp);
    const chatList = sortedChat.map(chat =>
      (chat.user_id === this.props.userId ? (
        <View style={styles.rightChat} key={chat.timestamp}>
          <Text style={styles.chatCluster}>
            {this.showChatClustering(chat.timestamp)}
          </Text>
          <View style={styles.chatInfo}>
            <View style={styles.chatInfoCol}>
              <View style={styles.chatInfoColItems}>
                {chat.read_by && (
                  <Text>{`Read ${Object.keys(chat.read_by).length}`} </Text>
                )}
                <Text>{this.getTimeFromDate(chat.timestamp)} </Text>
              </View>
            </View>
            <ChatBubbleRight
              userId={chat.user_id}
              key={chat.timestamp}
              type={chat.message.type}
            >
              {chat.message.content}
            </ChatBubbleRight>
          </View>
        </View>
      ) : (
        <View style={styles.leftChat} key={chat.timestamp}>
          <Text style={styles.chatCluster}>
            {this.showChatClustering(chat.timestamp)}
          </Text>
          <View style={styles.chatInfo}>
            <ChatBubbleLeft
              userId={chat.user_id}
              key={chat.timestamp}
              type={chat.message.type}
            >
              {chat.message.content}
            </ChatBubbleLeft>
            <View style={styles.chatInfoCol}>
              <View style={styles.chatInfoColItems}>
                <Text> {this.getTimeFromDate(chat.timestamp)}</Text>
              </View>
            </View>
          </View>
        </View>
      )));

    return (
      <ScrollView
        style={styles.contentContainer}
        ref={(ref) => {
          this.scrollView = ref;
        }}
        onContentSizeChange={() => {
          this.scrollView.scrollToEnd({ animated: false });
        }}
      >
        {chatList}
      </ScrollView>
    );
  }
}

ScrollbarPanel.propTypes = {
  userId: PropTypes.string.isRequired,
  groupId: PropTypes.string,
  chats: PropTypes.array,
  unreads: PropTypes.object,
  setReadBy: PropTypes.func.isRequired,
  getChats: PropTypes.func.isRequired
};

ScrollbarPanel.defaultProps = {
  chats: [],
  unreads: {},
  groupId: ''
};

function mapStateToProps(state) {
  return {
    groupId: state.group.selected,
    userId: state.user.activeId,
    unreads: state.user.unreads
  };
}

const styles = StyleSheet.create({
  contentContainer: {
    height: deviceHeight * 0.78,
    flex: 8,
    padding: 10,
    flexDirection: 'column'
  },
  timeStamp: {
    color: '#c8cecb',
    fontSize: 12
  },
  chatCluster: {
    color: 'black',
    textAlign: 'center',
    backgroundColor: 'white',
    width: deviceWidth * 0.09,
    borderBottomLeftRadius: 7,
    borderBottomRightRadius: 7,
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 7
  },
  rightChat: {
    flex: 1,
    alignItems: 'flex-end',
    flexDirection: 'column'
  },
  leftChat: {
    flex: 1,
    alignItems: 'flex-start',
    flexDirection: 'column'
  },
  chatInfo: {
    flex: 1,
    alignItems: 'flex-end',
    flexDirection: 'row'
  },
  chatInfoCol: {
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  chatInfoColItems: {
    flex: 1,
    alignItems: 'flex-end',
    flexDirection: 'column'
  }
});

export default connect(mapStateToProps, { setReadBy, getChats })(ScrollbarPanel);
