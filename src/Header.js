import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, Dimensions } from 'react-native';

const deviceHeight = Dimensions.get('window').height;
export default class Header extends React.Component {
  render() {
    return (
      <View style={styles.header}>
        <Text style={styles.title}>{this.props.children}</Text>
      </View>
    );
  }
}

Header.propTypes = {
  children: PropTypes.array
};

Header.defaultProps = {
  children: []
};

const styles = StyleSheet.create({
  header: {
    height: deviceHeight * 0.1,
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#02b0b9'
  },
  title: {
    marginLeft: 10,
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 24
  }
});
