import firebasemock from 'firebase-mock';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import proxyquire from 'proxyquire';
import * as types from '../../types';

// Mock Redux
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

// Mock Firebase
const mockdatabase = new firebasemock.MockFirebase();
const mocksdk = firebasemock.MockFirebaseSdk(path => (path ? mockdatabase.child(path) : mockdatabase));
const mockapp = mocksdk.initializeApp();

let firebase;
let userMock;
let actions;

describe('Group Actions', () => {
  beforeAll(() => {
    mockdatabase.autoFlush();
    actions = proxyquire('../../actions/group', {
      firebase: () => mockapp
    });
    jest.mock('../../firebase', () => mocksdk);
    firebase = require('../../firebase');
    userMock = firebase.database().ref();

    userMock.set({
      user: {
        123: {
          groups: ['L8ghPbwEg8CFA11nORg', 'L8hoCPbTa3Ijsj1v1g6'],
          img_url: 'hehe1111',
          name: 'Edward the Black Prince'
        }
      },
      group: {
        L8ghPbwEg8CFA11nORg: {
          group_name: 'Group 1',
          last_message: 'Hehehe',
          chats: [
            {
              message: {
                content: 'Asdf',
                type: 'text'
              },
              timestamp: 1523251984407,
              user_id: '123'
            }
          ],
          members: ['abcd', 'defg', 'hijk']
        },
        L8hoCPbTa3Ijsj1v1g6: {
          group_name: 'Group 2',
          last_message: 'wkwkwkwk'
        }
      }
    });
  });

  it('should dispatch member lists', () => {
    const memberList = ['abcd', 'defg', 'hijk'];
    const groupId = 'L8ghPbwEg8CFA11nORg';
    const store = mockStore({});

    store.dispatch(actions.getMemberList(groupId)).then((list) => {
      expect(list).toEqual(memberList);
    });
  });

  it('should dispatch selected group on function selectGroup()', () => {
    const groupId = 'L8ghPbwEg8CFA11nORg';
    const userId = '123';
    const chats = [
      {
        message: {
          content: 'Asdf',
          type: 'text'
        },
        timestamp: 1523251984407,
        user_id: '123',
        user: {
          groups: ['L8ghPbwEg8CFA11nORg', 'L8hoCPbTa3Ijsj1v1g6'],
          img_url: 'hehe1111',
          name: 'Edward the Black Prince'
        }
      }
    ];
    const expectedAction = [
      {
        type: types.CLOSE_MEMBER_TOGGLE
      },
      {
        type: types.GROUP_SELECTED,
        groupId
      },
      {
        type: types.CHAT_LOADED,
        chats
      }
    ];

    const store = mockStore({});

    store.dispatch(actions.selectGroup(userId, groupId));
    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch selected group ID', () => {
    const groupId = 'hahahay';
    const expectedAction = {
      type: types.GROUP_SELECTED,
      groupId
    };
    expect(actions.groupSelected(groupId)).toEqual(expectedAction);
  });

  it('should dispatch loaded groups', () => {
    const groups = {
      hahahay: { nama: 'bbb', last_message: 'hahahihi' },
      wadidaw: { nama: 'asw', last_message: 'wedew' }
    };
    const expectedAction = {
      type: types.GROUP_LOADED,
      groups
    };
    expect(actions.groupLoaded(groups)).toEqual(expectedAction);
  });
});
