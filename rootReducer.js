import { combineReducers } from 'redux';
import group from './reducers/group';
import chat from './reducers/chat';
import user from './reducers/user';
import meta from './reducers/meta';

export default combineReducers({
  group,
  chat,
  user,
  meta
});
