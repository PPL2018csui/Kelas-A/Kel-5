import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { userIdSet } from '../actions/user';

class LoginPage extends Component {
  constructor() {
    super();
    this.state = {
      data: {
        userName: ''
      }
    };
  }

  render() {
    const pic = {
      uri: '../assets/panel_first.png'
    };
    const { data } = this.state;

    return (
      <View style={styles.loginPanel}>
        <View style={styles.imageWrapper}>
          <Image
            style={styles.firstViewPic}
            source={pic}
            resizeMode="contain"
          />
        </View>
        <View style={styles.loginForm}>
          <TextInput
            style={styles.input}
            placeholder="username"
            value={data.userName}
            name="userName"
            onChangeText={text =>
              this.setState({ data: { ...data, userName: text } })
            }
          />
          <TouchableOpacity
            onPress={() => {
              this.props.userIdSet(this.state.data.userName);
            }}
          >
            <Text style={styles.loginButton}>Login</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

LoginPage.propTypes = {
  userIdSet: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    userId: state.user.activeId
  };
}

const styles = StyleSheet.create({
  loginPanel: {
    flex: 1,
    backgroundColor: '#f1f3f2',
    width: '100vw',
    height: '100vh'
  },
  loginForm: {
    width: '50%',
    alignSelf: 'center',
    marginBottom: 'auto',
    padding: 30,
    backgroundColor: '#02b0b9'
  },
  imageWrapper: {
    marginTop: 'auto'
  },
  firstViewPic: {
    alignSelf: 'center',
    width: '300px',
    height: '300px'
  },
  input: {
    width: '75%',
    alignSelf: 'center',
    padding: 10,
    marginBottom: 10,
    backgroundColor: 'white'
  },
  loginButton: {
    flex: 1,
    alignSelf: 'center',
    textAlign: 'center',
    fontWeight: 'bold',
    width: '75%',
    backgroundColor: 'grey',
    color: 'white',
    padding: 10
  }
});

export default connect(mapStateToProps, { userIdSet })(LoginPage);
