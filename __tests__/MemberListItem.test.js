import React from 'react';
import { View, Text, Image } from 'react-native';
import MemberListItem from '../src/MemberListItem';
import configureStore from 'redux-mock-store';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';
import { expect } from 'chai';

Enzyme.configure({ adapter: new Adapter() });

const initialState = {
  group: {
    selected: ''
  },
  user: {
    activeId: ''
  },
  profilepicture: {
    selected: ''
  },
};

// here it is possible to pass in any middleware if needed into //configureStore
const mockStore = configureStore();
let store;
beforeEach(() => {
  // creates the store with any initial state or middleware needed
  store = mockStore(initialState);
});

describe('<MemberListItem>', () => {
  test('should be like', () => {
    const component = shallow(<MemberListItem store={store} />);
    expect(component);
  });

  test('View should be like', () => {
    const view = renderer.create(<MemberListItem />);
    expect(view).to.include(View);
  });

  test('Text should be like', () => {
    const text = renderer.create(<MemberListItem />);
    expect(text).to.include(Text);
  });

  test('Image should be like', () => {
    const image = renderer.create(<MemberListItem />);
    expect(image).to.include(Image);
  });
});
