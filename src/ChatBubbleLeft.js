import React from 'react';
import {
  Text, // paragraph
  View, // div
  Image,
  StyleSheet,
  Dimensions
} from 'react-native';
import ViewThumbnail from './ViewThumbnail';
import { createElement } from 'react-native-web';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getUserById } from '../actions/user';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const Link = props => createElement('a', { ...props, target: '_blank' });

class ChatBubbleLeft extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {}
    };
  }
  componentWillMount() {
    this.props
      .getUserById(this.props.userId)
      .then(user => this.setState({ user }));
  }

  render() {
    const { user } = this.state;
    return (
      <View style={styles.bigContainer}>
        <View style={styles.container}>
          <View style={styles.imgName}>
            <Image
              style={styles.pictureImg}
              source={user.img_url}
              resizeMode="contain"
            />
            <View style={styles.name}>
              <View style={styles.name}>
                <Text style={styles.textNameStyle}> {user.name} </Text>
              </View>
              <View style={styles.messageBubble}>
                {this.props.type.match('^text$') && (
                  <Text style={styles.textStyle}>{this.props.children}</Text>
                )}
                {this.props.type.match('^image') && (
                  <ViewThumbnail src={this.props.children} />
                )}
                {!this.props.type.match('^image') &&
                  !this.props.type.match('^text$') && (
                  <Link href={this.props.children}>
                    {this.props.children.split('-')[1]}
                  </Link>
                )}
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

ChatBubbleLeft.propTypes = {
  getUserById: PropTypes.func.isRequired,
  children: PropTypes.string,
  userId: PropTypes.string,
  type: PropTypes.string
};

ChatBubbleLeft.defaultProps = {
  children: '',
  userId: '',
  type: ''
};

const styles = StyleSheet.create({
  bigContainer: {
    flex: 1,
    alignItems: 'flex-start'
  },
  container: {
    flex: 1,
    alignItems: 'flex-end'
  },
  imgName: {
    flexDirection: 'row',
    alignSelf: 'flex-start'
  },
  name: {
    flex: 1,
    alignSelf: 'flex-start'
  },
  pictureImg: {
    width: deviceHeight / 12,
    height: deviceHeight / 12
  },
  messageBubble: {
    flex: 1,
    borderBottomLeftRadius: 14,
    borderBottomRightRadius: 14,
    borderTopRightRadius: 14,
    alignSelf: 'flex-start',
    paddingHorizontal: 7,
    paddingVertical: 7,
    backgroundColor: 'white',
    maxWidth: deviceWidth / 4,
    shadowColor: 'grey',
    shadowRadius: 1,
    shadowOpacity: 1,
    shadowOffset: { width: 1, height: 1 }
  },
  textStyle: {
    color: 'black',
    fontSize: 14
  },
  textNameStyle: {
    color: 'grey',
    fontSize: 13
  }
});

export default connect(null, { getUserById })(ChatBubbleLeft);
