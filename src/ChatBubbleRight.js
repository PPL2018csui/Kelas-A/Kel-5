import React from 'react';
import PropTypes from 'prop-types';
import ViewThumbnail from './ViewThumbnail';
import { Text, View, StyleSheet, Dimensions, Image } from 'react-native';
import { createElement } from 'react-native-web';
import { connect } from 'react-redux';
import { getUserById } from '../actions/user';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const Link = props => createElement('a', { ...props, target: '_blank' });
const pic = '../assets/file_icon.png';

class ChatBubbleRight extends React.Component {
  render() {
    return (
      <View style={styles.bigContainer}>
        <View style={styles.container}>
          <View style={styles.name}>
            <View style={styles.messageBubble}>
              {this.props.type.match('^text$') && (
                <Text style={styles.textStyle}>{this.props.children}</Text>
              )}
              {this.props.type.match('^image') && (
                <ViewThumbnail src={this.props.children} />
              )}
              {!this.props.type.match('^image') &&
                !this.props.type.match('^text$') && (
                <Link href={this.props.children}>
                  <View style={styles.linking}>
                    <Image
                      style={styles.picIcon}
                      source={pic}
                      resizeMode="contain"
                    />
                    <Text style={styles.textLink}>
                      {this.props.children.split('-')[1]}
                    </Text>
                  </View>
                </Link>
              )}
            </View>
          </View>
        </View>
      </View>
    );
  }
}

ChatBubbleRight.propTypes = {
  children: PropTypes.string,
  type: PropTypes.string
};

ChatBubbleRight.defaultProps = {
  children: '',
  type: ''
};

const styles = StyleSheet.create({
  bigContainer: {
    flex: 1,
    alignItems: 'flex-end'
  },
  container: {
    flex: 1,
    alignItems: 'flex-start'
  },
  imgName: {
    flexDirection: 'row',
    alignSelf: 'flex-end'
  },
  name: {
    flex: 1,
    alignSelf: 'flex-end'
  },
  pictureImg: {
    width: deviceHeight / 12,
    height: deviceHeight / 12
  },
  messageBubble: {
    flex: 1,
    borderBottomLeftRadius: 14,
    borderBottomRightRadius: 14,
    borderTopLeftRadius: 14,
    alignSelf: 'flex-end',
    paddingHorizontal: 7,
    paddingVertical: 7,
    backgroundColor: '#02b0b9',
    maxWidth: deviceWidth / 4,
    shadowColor: 'grey',
    shadowRadius: 1,
    shadowOpacity: 50,
    shadowOffset: { width: 1, height: 1 }
  },
  textStyle: {
    color: 'white',
    fontSize: 14
  },
  linking: {
    height: 30,
    flexDirection: 'row'
  },
  textLink: {
    color: 'white',
    marginTop: 'auto',
    marginBottom: 'auto'
  },
  picIcon: {
    width: 25,
    height: 25,
    padding: 3,
    marginTop: 'auto',
    marginBottom: 'auto'
  }
});

export default connect(null, { getUserById })(ChatBubbleRight);
