import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ChatBubbleLeft from './ChatBubbleLeft';
import ChatBubbleRight from './ChatBubbleRight';

class ChatBubble extends React.Component {
  render() {
    const { chat, userId } = this.props;
    const bubble =
      chat.user_id === userId ? (
        <ChatBubbleRight user={chat.user} type={chat.message}>
          {chat.message.content}
        </ChatBubbleRight>
      ) : (
        <ChatBubbleLeft user={chat.user} type={chat.message}>
          {chat.message.content}
        </ChatBubbleLeft>
      );

    return { bubble };
  }
}

ChatBubble.propTypes = {
  chat: PropTypes.shape().isRequired,
  userId: PropTypes.string.isRequired
};

function mapStateToProps(state) {
  return {
    userId: state.user.activeId
  };
}

export default connect(mapStateToProps)(ChatBubble);
