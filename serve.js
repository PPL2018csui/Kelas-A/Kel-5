const express = require('express');
const path = require('path');
const upload = require('./upload');
const group = require('./group');

const PORT = process.env.PORT || 5000;
const app = express();

app.use(express.static('public'));
app.use('/files', express.static('files'));
app.use('/assets', express.static('assets'));
app.use('/api/upload', upload);
app.use('/api/group', group);
app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.listen(PORT, () => console.log(`Listening on ${PORT}`));
