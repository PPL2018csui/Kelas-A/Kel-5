import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { createElement } from 'react-native-web';
import { connect } from 'react-redux';
import { Image, View, StyleSheet } from 'react-native';

const InputFile = props => createElement('input', { ...props, type: 'file' });

class UploadButton extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    // this.setState({ file: e.target.files[0] });
    const url = '/api/upload';
    const formData = new FormData();
    formData.append('file', e.target.files[0]);
    formData.append('userId', this.props.userId);
    formData.append('groupId', this.props.groupId);
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    };
    axios.post(url, formData, config);
  }

  moveToServer() {
    return true;
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.picture}
          source={{
            uri: '../assets/arrow.png'
          }}
        />
        <InputFile style={styles.input} onChange={this.onChange} />
      </View>
    );
  }
}

UploadButton.propTypes = {
  userId: PropTypes.string,
  groupId: PropTypes.string
};

UploadButton.defaultProps = {
  userId: '',
  groupId: ''
};

const styles = StyleSheet.create({
  picture: {
    width: 45,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    borderRadius: 5,
    padding: 'auto',
    alignSelf: 'flex-end',
    margin: 5
  },
  container: {
    width: 45,
    height: 45,
    position: 'relative',
    overflow: 'hidden'
  },
  input: {
    fontSize: 100,
    position: 'absolute',
    left: 0,
    top: 0,
    opacity: 0
  }
});

function mapStateToProps(state) {
  return {
    userId: state.user.activeId,
    groupId: state.group.selected
  };
}

export default connect(mapStateToProps)(UploadButton);
