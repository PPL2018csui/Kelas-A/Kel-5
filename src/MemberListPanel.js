import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Dimensions, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { getMemberList } from '../actions/group';
import { getMemberByIds } from '../actions/user';
import MemberListItem from './MemberListItem';

const deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  listPanel: {
    flex: 7,
    maxHeight: deviceHeight * 0.9
  },
  containerfull: {
    flex: 1,
  },
  titleGroup: {
    alignSelf: 'center',
    fontSize: 22,
    padding: 20,
    fontWeight: 'bold',
  }
});

class MemberListPanel extends Component {
  constructor(props) {
    super(props);
    this.state = { members: [] };
  }

  componentDidMount() {
    this.props.getMemberList(this.props.groupId).then((memberIds) => {
      this.props.getMemberByIds(memberIds).then((members) => {
        this.setState({ members });
      });
    });
  }

  render() {
    const { members } = this.state;
    const memberItems = members.map(member => (
      <MemberListItem key={member.id} member={member} />
    ));
    return (
      <ScrollView style={styles.listPanel}>
        <Text style={styles.titleGroup}>Member of this group</Text>
        <View style={styles.containerfull}>{memberItems}</View>
      </ScrollView>
    );
  }
}

MemberListPanel.propTypes = {
  getMemberList: PropTypes.func.isRequired,
  groupId: PropTypes.string.isRequired,
  getMemberByIds: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    groupId: state.group.selected
  };
}

export default connect(mapStateToProps, { getMemberList, getMemberByIds })(MemberListPanel);
