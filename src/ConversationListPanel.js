import React from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import ConversationItemPanel from './ConversationItemPanel';
import HeaderConversation from './HeaderConversation';

class ConversationListPanel extends React.Component {
  render() {
    return (
      <View style={styles.conversationListPanel}>
        <HeaderConversation title={this.props.activeId} />
        <ConversationItemPanel />
      </View>
    );
  }
}

ConversationListPanel.propTypes = {
  activeId: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  activeId: state.user.activeId
});

const styles = StyleSheet.create({
  conversationListPanel: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: 'white',
  }
});

export default connect(mapStateToProps)(ConversationListPanel);
