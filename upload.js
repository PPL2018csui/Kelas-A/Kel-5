const express = require('express');
const multer = require('multer');
const firebase = require('./firebase');

const router = express.Router();
const storage = multer.diskStorage({
  destination: './files',
  filename(req, file, cb) {
    cb(null, `${new Date().getTime()}-${file.originalname}`);
  }
});

const upload = multer({ storage });

router.post('/', upload.single('file'), (req, res) => {
  const { file, body } = req;
  let chatLength = 0;
  const chatRef = firebase
    .database()
    .ref()
    .child(`group/${body.groupId}/chats`);

  const chatContent = {
    message: {
      content: file.path,
      type: file.mimetype
    },
    timestamp: new Date().getTime(),
    user_id: body.userId
  };

  // Set array key
  chatRef.once('value', (snap) => {
    if (snap.val() !== undefined) {
      chatLength = snap.val().length;
    }

    // Insert chat to array
    firebase
      .database()
      .ref()
      .child(`group/${body.groupId}/chats/${chatLength}`)
      .set(chatContent);
  });

  // Set last message of the group
  firebase
    .database()
    .ref()
    .child(`group/${body.groupId}/last_message`)
    .set(chatContent);

  res.json({ file: file });
});

module.exports = router;
