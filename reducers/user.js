import {
  GROUP_ID_LOADED,
  UNREAD_LOADED,
  UNREAD_CLEARED,
  USER_ID_SET
} from '../types';

export default function user(
  state = { activeId: '', groupIds: [], unreads: {} },
  action = {}
) {
  switch (action.type) {
  case GROUP_ID_LOADED:
    return { ...state, groupIds: action.groupId };
  case UNREAD_LOADED:
    return { ...state, unreads: action.unreads };
  case UNREAD_CLEARED: {
    const newUnread = { ...state.unread };
    delete newUnread[action.groupId];
    return { ...state, unreads: newUnread };
  }
  case USER_ID_SET:
    return { ...state, activeId: action.userId };
  default:
    return state;
  }
}
