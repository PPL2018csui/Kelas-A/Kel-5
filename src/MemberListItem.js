import React from 'react';
import { StyleSheet, View, Text, Image, Dimensions } from 'react-native';
import PropTypes from 'prop-types';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  picture: {
    width: deviceHeight / 12,
    height: deviceHeight / 12,
  },
  container: {
    padding: 10,
    borderBottomWidth: 0.5,
    borderColor: '#d6d7da',
    flex: 1
  },
  container1: {
    flex: 1,
    marginLeft: deviceWidth * 0.03,
    flexDirection: 'row',
    alignSelf: 'flex-start',
  },
  font: {
    fontSize: 17,
    padding: 10,
  },
});

export default class MemberListItem extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.container1}>
          <Image
            style={styles.picture}
            source={{
              uri:
              this.props.member.img_url
            }}
          />
          <Text style={styles.font}>{this.props.member.name}</Text>
        </View>
      </View>
    );
  }
}

MemberListItem.propTypes = {
  member: PropTypes.shape({
    name: PropTypes.string,
    img_url: PropTypes.string
  })
};

MemberListItem.defaultProps = {
  member: {
    name: ''
  }
};
