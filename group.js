const express = require('express');
const firebase = require('./firebase');
const bodyParser = require('body-parser');

const router = express.Router();
router.use(bodyParser.json());
router.post('/create', (req, res) => {
  const groupContent = {
    group_name: req.body.name,
    img_url: req.body.img,
    last_message: {
      message: {
        type: 'text'
      },
      timestamp: new Date().getTime()
    },
    members: req.body.members
  };

  firebase
    .database()
    .ref()
    .child('group')
    .push(groupContent)
    .then((snap) => {
      req.body.members.forEach((member) => {
        const groupRef = firebase
          .database()
          .ref()
          .child(`user/${member}/groups`);

        groupRef.once('value', (snapshot) => {
          const arrGroup = snapshot.val();
          arrGroup.push(snap.key);

          groupRef.set(arrGroup);
        });
      });

      res.json({});
    });
});

router.post('/add-member', (req, res) => {
  const memberRef = firebase
    .database()
    .ref()
    .child(`group/${req.body.group_id}/members`);

  memberRef.once('value', (snap) => {
    const arrMember = snap.val();

    memberRef.set(arrMember.concat(req.body.members));
  });

  req.body.members.forEach((member) => {
    const groupRef = firebase
      .database()
      .ref()
      .child(`user/${member}/groups`);

    groupRef.once('value', (snap) => {
      const arrGroup = snap.val();
      arrGroup.push(req.body.group_id);

      groupRef.set(arrGroup);
    });
  });

  res.json({});
});

module.exports = router;
